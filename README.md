# TP5-conception-logicielle

## Installer les dépendances 

`pip3 install -r requirements.txt`

## Lancer l'application

`python3 main.py`

## Lancer la FastAPI

`uvicorn main:app`

## Lancer les tests unitaires

Commencez par sauvegarder les fichiers json rozana et brioche :

```
curl https://world.openfoodfacts.org/api/v0/product/3256540001305 > brioche.json
curl https://world.openfoodfacts.org/api/v0/product/3468570116601 > rozana.json
```

Puis lancer :

`pytest`