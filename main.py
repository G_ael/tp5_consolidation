from typing import Optional

from fastapi import FastAPI

import requests

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}

requete = requests.get("https://world.openfoodfacts.org/api/v0/product/3256540001305.json")
print(requete.status_code)
print(requete.json)

def print_item(item_id):
    requete = requests.get("https://world.openfoodfacts.org/api/v0/product/" + item_id + ".json")
    return(requete.json())


item1 = print_item("3256540001305")
#print(item1)

def isVegan(requestAsJson):
    ingredients = requestAsJson["product"]["ingredients"]
    for ingredient in ingredients:
        if 'vegan' in ingredient:
          if ingredient["vegan"]=='no' or ingredient["vegan"]=='maybe':
            return False
    return True


